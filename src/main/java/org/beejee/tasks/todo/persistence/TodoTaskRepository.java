package org.beejee.tasks.todo.persistence;

import org.beejee.tasks.todo.domain.TodoTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
public interface TodoTaskRepository extends JpaRepository<TodoTask,Long> {

    Page<TodoTask> findAllByOrderByEmail(Pageable pageable);

    Page<TodoTask> findAllByOrderByUserName(Pageable pageable);

    Page<TodoTask> findAllByOrderByCompleted(Pageable pageable);

}
