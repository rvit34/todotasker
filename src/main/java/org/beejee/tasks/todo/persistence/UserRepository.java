package org.beejee.tasks.todo.persistence;

import org.beejee.tasks.todo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
public interface UserRepository extends JpaRepository<User,String> {}
