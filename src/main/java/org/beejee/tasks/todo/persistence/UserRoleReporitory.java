package org.beejee.tasks.todo.persistence;

import org.beejee.tasks.todo.domain.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
public interface UserRoleReporitory extends JpaRepository<UserRole,Long> {
}
