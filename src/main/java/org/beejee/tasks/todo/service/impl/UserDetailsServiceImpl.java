package org.beejee.tasks.todo.service.impl;

import org.beejee.tasks.todo.domain.User;
import org.beejee.tasks.todo.domain.UserRole;
import org.beejee.tasks.todo.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User user = userRepository.findOne(userName);
        if (user == null) throw new UsernameNotFoundException(String.format("user %s is not found", userName));

        return new org.springframework.security.core.userdetails.User(
                user.getName(),
                user.getPassword(),
                getAutority(user.getRoles())
        );
    }

    private Collection<? extends GrantedAuthority> getAutority(Set<UserRole> roles) {
        Set<GrantedAuthority> authSet = new HashSet<>();

        for (UserRole role:roles){
            authSet.add(new SimpleGrantedAuthority(role.getName()));
        }

        return authSet;
    }


}
