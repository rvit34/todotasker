package org.beejee.tasks.todo.service.impl;

import org.beejee.tasks.todo.domain.TodoTask;
import org.beejee.tasks.todo.domain.User;
import org.beejee.tasks.todo.domain.UserRole;
import org.beejee.tasks.todo.persistence.TodoTaskRepository;
import org.beejee.tasks.todo.persistence.UserRepository;
import org.beejee.tasks.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by Vitaly Romashkin on 09.07.2017.
 */
@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoTaskRepository todoTaskRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public TodoTask addNewTodo(TodoTask task) {

        User user = task.getUser();
        if (user == null) throw new IllegalStateException("task does not contain user info");

        if (userRepository.findOne(user.getName()) == null){
            if (user.getRoles().isEmpty()){
                user.addRole(UserRole.USER);
            }
            userRepository.save(user);
        }

        return todoTaskRepository.save(task);
    }

    @Override
    public void updateDescription(String newDescription, long todoId) {
        TodoTask todoTask = todoTaskRepository.findOne(todoId);
        if (todoTask == null) throw new IllegalArgumentException(String.format("todo with id=%s is not found", todoId));

        todoTask.setDescription(newDescription);
        todoTaskRepository.save(todoTask);
    }

    @Override
    public void updateStatus(boolean completed, long todoId) {
        TodoTask todoTask = todoTaskRepository.findOne(todoId);
        if (todoTask == null) throw new IllegalArgumentException(String.format("todo with id=%s is not found", todoId));

        todoTask.setCompleted(completed);
        todoTaskRepository.save(todoTask);
    }

    @Override
    public Page<TodoTask> getAllTodos(int pageNumber, int pageSize) {
        return todoTaskRepository.findAll(new PageRequest(pageNumber,pageSize));
    }

    @Override
    public Page<TodoTask> getAllTodosOrderByUserName(int pageNumber, int pageSize) {
        return todoTaskRepository.findAllByOrderByUserName(new PageRequest(pageNumber,pageSize));
    }

    @Override
    public Page<TodoTask> getAllTodosOrderByEmail(int pageNumber, int pageSize) {
        return todoTaskRepository.findAllByOrderByEmail(new PageRequest(pageNumber,pageSize));
    }

    @Override
    public Page<TodoTask> getAllTodosOrderByStatus(int pageNumber, int pageSize) {
        return todoTaskRepository.findAllByOrderByCompleted(new PageRequest(pageNumber,pageSize));
    }


}
