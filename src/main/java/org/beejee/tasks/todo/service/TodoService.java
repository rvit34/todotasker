package org.beejee.tasks.todo.service;

import org.beejee.tasks.todo.domain.TodoTask;
import org.springframework.data.domain.Page;


/**
 * Created by Vitaly Romashkin on 09.07.2017.
 */
public interface TodoService {

    TodoTask addNewTodo(TodoTask task);

    void updateDescription(String newDescription, long todoId);

    Page<TodoTask> getAllTodos(int pageNumber, int pageSize);

    Page<TodoTask> getAllTodosOrderByUserName(int pageNumber, int pageSize);

    Page<TodoTask> getAllTodosOrderByEmail(int pageNumber, int pageSize);

    Page<TodoTask> getAllTodosOrderByStatus(int pageNumber, int pageSize);

    void updateStatus(boolean completed, long id);
}
