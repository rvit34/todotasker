package org.beejee.tasks.todo.ui;

/**
 * Created by Vitaly Romashkin on 09.07.2017.
 */

import com.vaadin.addon.pagination.Pagination;
import com.vaadin.addon.pagination.PaginationChangeListener;
import com.vaadin.addon.pagination.PaginationResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import lombok.extern.slf4j.Slf4j;
import org.beejee.tasks.todo.domain.TodoTask;
import org.beejee.tasks.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.List;

@Slf4j
public class ToDoListPage extends UI {

    @Autowired
    protected TodoService todoService;

    private Pagination pagination;
    private int currentPage = 0, currentLimit = 3;

    private Grid grid;

    protected void init(VaadinRequest vaadinRequest) {
        setContent(setupGrigLayout());
    }


    public VerticalLayout setupGrigLayout() {

        final Page<TodoTask> tasks = todoService.getAllTodos(0, currentLimit);
        final long total = tasks.getTotalElements();

        grid = createGrid(tasks.getContent());
        pagination = createPagination(total, currentPage, currentLimit);
        pagination.addPageChangeListener((PaginationChangeListener) event -> {
            log.debug("page change event : {}", event.toString());
            currentPage = event.pageIndex();
            currentLimit = event.limit();
            updateGrid();
        });

        Button addNewTaskButton = new Button("New task", clickEvent -> addNewTask());
        addNewTaskButton.setStyleName(ValoTheme.BUTTON_FRIENDLY,true);

        final VerticalLayout layout = createContent(addNewTaskButton, grid, pagination);
        return layout;
    }

    private void updateGrid() {
        Page<TodoTask> tasks = loadPage(currentPage, currentLimit);
        pagination.setTotalCount(tasks.getTotalElements());
        grid.setItems(tasks.getContent());
        grid.scrollToStart();
    }

    private void addNewTask() {
        addWindow(new NewTaskWindow(task -> {
            todoService.addNewTodo(task);
            updateGrid();
        }));
    }


    public Page<TodoTask> loadPage(int page, int size) {
        return todoService.getAllTodos(page,size);
    }

    protected Grid<TodoTask> createGrid(List<TodoTask> tasks) {
        return new TaskGrid(tasks);
    }

    private Pagination createPagination(long total, int page, int limit) {
        final PaginationResource paginationResource = PaginationResource.newBuilder().setTotal(total).setPage(page).setLimit(limit).build();
        final Pagination pagination = new Pagination(paginationResource);
        pagination.setItemsPerPage(3, 5, 10, 20);
        return pagination;
    }

    private VerticalLayout createContent(Button button, Grid grid, Pagination pagination) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setSpacing(true);
        layout.addComponents(button, grid, pagination);
        layout.setExpandRatio(grid, 1f);
        return layout;
    }




}


