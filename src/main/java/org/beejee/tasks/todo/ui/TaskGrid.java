package org.beejee.tasks.todo.ui;

import com.vaadin.data.ValueProvider;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.ImageRenderer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.beejee.tasks.todo.domain.TodoTask;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by Vitaly Romashkin on 13.07.2017.
 */
@Slf4j
public class TaskGrid extends Grid<TodoTask> {

    public TaskGrid(List<TodoTask> tasks) {

        setItems(tasks);
        setSizeFull();
        addColumn((ValueProvider<TodoTask, String>) task -> task.getUser().getName()).setCaption("User");
        addColumn(TodoTask::getEmail).setCaption("Email");
        addColumn(TodoTask::getDescription)
                .setCaption("Task")
                .setWidthUndefined()
                .setId("Task");
        addColumn(task -> task.isCompleted() ? "done" : "new")
                .setCaption("Status")
                .setId("Status");
        addColumn(task -> {
            Embedded embeded = new Embedded();
            if (task.getImagePath() == null) return new ExternalResource("");

            File file = new File(task.getImagePath());
            embeded.setSource(new FileResource(file));
            String type = embeded.getMimeType();

            String format = type.substring(type.indexOf('/')+1);


            StringBuilder dataBuilder = new StringBuilder();
            dataBuilder.append("data:");
            dataBuilder.append(type+";");
            dataBuilder.append("base64,");
            dataBuilder.append( getScaledImageForRender(file,format, 100, 100));

            return new ExternalResource(dataBuilder.toString());
        }, new ImageRenderer()).setCaption("Image");
    }

    private static String getScaledImageForRender(File file, String format, int targetWidth, int targetHeight){
        try {
            BufferedImage scaledImage = Scalr.resize(ImageIO.read(file),targetWidth,targetHeight);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(scaledImage,format,baos);
            byte[] data = baos.toByteArray();
            return new String(Base64.encodeBase64(data), StandardCharsets.US_ASCII);
        } catch (IOException e) {
            log.error("could not resize image {}",file.getName(),e);
            return new String();
        }
    }
}
