package org.beejee.tasks.todo.ui;

import org.beejee.tasks.todo.domain.TodoTask;

/**
 * Created by Vitaly Romashkin on 10.07.2017.
 */
@FunctionalInterface
public interface NewTaskListener {

    void newTask(TodoTask task);
}
