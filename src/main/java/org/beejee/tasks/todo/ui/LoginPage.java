package org.beejee.tasks.todo.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by Vitaly Romashkin on 09.07.2017.
 */
@SpringUI(path = "/login")
@Theme("valo")
@Slf4j
public class LoginPage extends UI {

    @Autowired
    AuthenticationManager manager;

    private TextField userNameTextField;
    private PasswordField passwordTextField;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
       layoutComponents();
    }

    private void layoutComponents(){

        VerticalLayout layout = new VerticalLayout();
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(layout);

        Panel panel = new Panel("Admin Login");
        panel.setSizeUndefined();

        userNameTextField = new TextField("Username");
        passwordTextField = new PasswordField("Password");

        Button loginButton = new Button("Enter");
        loginButton.addClickListener(clickEvent -> login());

        FormLayout loginLayout = new FormLayout();

        loginLayout.addComponent(userNameTextField);
        loginLayout.addComponent(passwordTextField);
        loginLayout.addComponent(loginButton);

        loginLayout.setSizeUndefined();
        loginLayout.setMargin(true);

        panel.setContent(loginLayout);

        layout.addComponent(panel);
        //setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
    }

    private void login() {

        String userName = userNameTextField.getValue();

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userName,passwordTextField.getValue());
        try {
            SecurityContextHolder.getContext().setAuthentication(manager.authenticate(authentication));
            log.info("user {} is logged in", userName);

            getUI().getPage().setLocation("/admin/todo");
        } catch (AuthenticationException e) {
            log.warn("could not authenticate user {}:{}", userName, e.getMessage());
            Notification.show(String.format("Could not authenticate user",e.getMessage()), Notification.Type.ERROR_MESSAGE);
        }
    }




}