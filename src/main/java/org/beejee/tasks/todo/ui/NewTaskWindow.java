package org.beejee.tasks.todo.ui;

import com.vaadin.server.FileResource;
import com.vaadin.ui.*;
import lombok.extern.slf4j.Slf4j;
import org.beejee.tasks.todo.domain.TodoTask;
import org.beejee.tasks.todo.domain.User;
import org.beejee.tasks.todo.domain.UserRole;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.validation.ValidationException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Created by Vitaly Romashkin on 10.07.2017.
 */
@Slf4j
public class NewTaskWindow extends Window {

    private TextField userNameTextField;

    private TextField emailTextField;

    private TextArea taskDescriptionTextArea;

    private Embedded image;

    private ProgressBar uploadProgressBar;

    private Button addButton;

    private Button previewButton;

    private NewTaskListener listener;

    public NewTaskWindow(NewTaskListener listener) {
         super();
         this.listener = listener;
         setupLayout();
         setClosable(true);
         setDraggable(true);
         setResizable(true);
         setModal(true);
         center();
         setCaption("New Task Window");
    }

    private void setupLayout() {

        userNameTextField = new TextField("user");
        userNameTextField.setMaxLength(16);

        emailTextField = new TextField("email");
        emailTextField.setMaxLength(20);

        taskDescriptionTextArea = new TextArea("task description");
        taskDescriptionTextArea.setWordWrap(true);
        taskDescriptionTextArea.setMaxLength(255);

        image = new Embedded("Uploaded Image");

        //image.setWidth(320,Unit.PIXELS);
        //image.setHeight(240,Unit.PIXELS);

        final UploadImageReceiver receiver = new UploadImageReceiver();

        Upload upload = new Upload("Upload Image Here", receiver);
        upload.addFailedListener(receiver);
        upload.addSucceededListener(receiver);
        upload.addFailedListener(receiver);
        upload.addStartedListener(receiver);
        upload.addProgressListener(receiver);

        uploadProgressBar = new ProgressBar();
        uploadProgressBar.setVisible(false);

        addButton = new Button("add");
        addButton.addClickListener(clickEvent -> addTask(receiver));

        previewButton = new Button("preview");
        previewButton.addClickListener(clickEvent -> preview(receiver));

        FormLayout formLayout = new FormLayout();
        formLayout.setMargin(true);

        formLayout.addComponent(userNameTextField);
        formLayout.addComponent(emailTextField);
        formLayout.addComponent(taskDescriptionTextArea);
        formLayout.addComponent(upload);
        formLayout.addComponent(image);
        formLayout.addComponent(uploadProgressBar);
        formLayout.addComponent(addButton);
        formLayout.addComponent(previewButton);

        formLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        setContent(formLayout);

    }

    private void preview(UploadImageReceiver receiver) {
        UI.getCurrent().addWindow(new PreviewWindow(getTask(receiver)));
    }

    private void addTask(UploadImageReceiver receiver) {
        try {
            validation();
        } catch (ValidationException e) {
            Notification.show("warn", e.getMessage(), Notification.Type.WARNING_MESSAGE);
            return;
        }
        listener.newTask(getTask(receiver)
        );
        close();
    }

    private TodoTask getTask(UploadImageReceiver receiver) {
        return new TodoTask(
                taskDescriptionTextArea.getValue(),
                emailTextField.getValue(),
                new User(userNameTextField.getValue(), UserRole.USER),
                receiver.file == null ? null: receiver.file.getAbsolutePath(),
                false
        );
    }

    private void validation() throws ValidationException {

        String userName = userNameTextField.getValue();
        String email = emailTextField.getValue();
        String task = taskDescriptionTextArea.getValue();

        if (userName == null || userName.isEmpty()){
            throw new ValidationException("user name cannot be empty");
        }

        final Pattern validEmailAddress =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        if (!validEmailAddress.matcher(email).find()){
            throw new ValidationException("incorrect email");
        }

        if (task == null || task.isEmpty()){
            throw new ValidationException("task desscription cannot be empty");
        }

    }

    private class UploadImageReceiver implements Upload.Receiver,Upload.FailedListener,Upload.SucceededListener,Upload.StartedListener, Upload.ProgressListener {

        private File file;

        private final String[] allowedMimeTypes = {"image/jpeg","image/jpg","image/png","image/gif"};

        @Override
        public void uploadFailed(Upload.FailedEvent failedEvent) {
            log.warn("could not upload an image:{}", failedEvent.getReason().getMessage());
            Notification.show("upload failed", failedEvent.getReason().getMessage(), Notification.Type.ERROR_MESSAGE);

            uploadProgressBar.setVisible(false);
            addButton.setEnabled(true);
            failedEvent.getUpload().setEnabled(true);
        }

        @Override
        public void uploadStarted(Upload.StartedEvent startedEvent) {

            String contentType = startedEvent.getMIMEType();
            boolean allowed = false;
            for(int i=0;i<allowedMimeTypes.length;i++){
                if(contentType.equalsIgnoreCase(allowedMimeTypes[i])){
                    allowed = true;
                    break;
                }
            }
            if(allowed){
                uploadProgressBar.setValue(0f);
                uploadProgressBar.setVisible(true);

                addButton.setEnabled(false);
                startedEvent.getUpload().setEnabled(false);
            }else{
                Notification.show(String.format("Type %s is not allowed. Allowed MIME types %s: ", contentType, Arrays.toString(allowedMimeTypes)), Notification.Type.WARNING_MESSAGE);
                startedEvent.getUpload().interruptUpload();
            }
        }

        @Override
        public OutputStream receiveUpload(String fileName, String mimeType) {
            file = new File(fileName);
            try {
                return new FileOutputStream(fileName);
            } catch (FileNotFoundException e) {
                log.error("could not create file output stream from file {}",fileName,e);
                return null;
            }
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent succeededEvent) {
            log.info("file {} was uploaded successfully", file.getAbsolutePath());

            scaleImageAndRewriteIfNeeded(file);
            image.setSource(new FileResource(file));

            uploadProgressBar.setVisible(false);
            addButton.setEnabled(true);
            succeededEvent.getUpload().setEnabled(true);
        }

        @Override
        public void updateProgress(long current, long all) {
             uploadProgressBar.setValue(current);
        }

        private void scaleImageAndRewriteIfNeeded(File file) {
            try {

                final int prefWidth = 320;
                final int prefHeight = 240;

                BufferedImage srcImage = ImageIO.read(file);
                int width = srcImage.getWidth();
                int height = srcImage.getHeight();

                if (height > prefHeight || width > prefWidth){
                    srcImage = Scalr.resize(srcImage,prefWidth,prefHeight);
                    String fileName = file.getName();
                    String format = fileName.substring(fileName.indexOf('.')+1);
                    ImageIO.write(srcImage,format,new FileOutputStream(file,false));
                }

            } catch (Exception e) {
                log.error("could not scale/write image {}",file.getName(),e);
            }
        }

    }


}
