package org.beejee.tasks.todo.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

/**
 * Created by Vitaly Romashkin on 10.07.2017.
 */
@SpringUI()
@Theme("valo")
public class RootUI extends UI {
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        getUI().getPage().setLocation("/todo");
    }
}
