package org.beejee.tasks.todo.ui;


import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;
import org.beejee.tasks.todo.domain.TodoTask;

import java.util.Arrays;

/**
 * Created by Vitaly Romashkin on 13.07.2017.
 */
public class PreviewWindow extends Window {

    private TaskGrid taskGrid;

    public PreviewWindow(TodoTask task) {

        super();
        setClosable(true);
        setDraggable(true);
        setResizable(true);
        setModal(true);
        center();
        setCaption("Preview");

        taskGrid = new TaskGrid(Arrays.asList(task));

        Panel panel = new Panel(taskGrid);
        setContent(panel);
        setWidth(100.0F, Unit.PERCENTAGE);
    }
}
