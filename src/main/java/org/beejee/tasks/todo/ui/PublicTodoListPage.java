package org.beejee.tasks.todo.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.spring.annotation.SpringUI;

/**
 * Created by Vitaly Romashkin on 10.07.2017.
 */
@SpringUI(path = "/todo")
@Theme("valo")
@Title("ToDo list")
public class PublicTodoListPage extends ToDoListPage {}
