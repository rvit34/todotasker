package org.beejee.tasks.todo.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Binder;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.EditorSaveEvent;
import lombok.extern.slf4j.Slf4j;
import org.beejee.tasks.todo.domain.TodoTask;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Vitaly Romashkin on 10.07.2017.
 */
@SpringUI(path = "/admin/todo")
@Theme("valo")
@Title("Admin ToDo list")
@Slf4j
public class AdminToDoListPage extends ToDoListPage {

    @Override
    protected Grid createGrid(List<TodoTask> tasks) {

        Grid<TodoTask> grid = super.createGrid(tasks);

        Grid.Column statusColumn = grid.getColumn("Status");
        Grid.Column<TodoTask,String> taskColumn = (Grid.Column<TodoTask, String>) grid.getColumn("Task");

        TextField taskEditArea = new TextField();
        CheckBox doneField = new CheckBox();

        Binder<TodoTask> binder = grid.getEditor().getBinder();

        Binder.Binding<TodoTask, Boolean> doneBinding = binder.bind(
                doneField, TodoTask::isCompleted, TodoTask::setCompleted);

        statusColumn.setEditorBinding(doneBinding);
        taskColumn.setEditorComponent(taskEditArea, TodoTask::setDescription).setExpandRatio(1);

        grid.getEditor().setEnabled(true);
        grid.getEditor().addSaveListener(editorSaveEvent -> {
            TodoTask editedTask = getEditedTask(editorSaveEvent);
            if (editedTask != null){
                todoService.updateDescription(editedTask.getDescription(),editedTask.getId());
                todoService.updateStatus(editedTask.isCompleted(), editedTask.getId());
            }
        });

        taskColumn.setEditable(true);
        statusColumn.setEditable(true);

        return grid;
    }

    private TodoTask getEditedTask(EditorSaveEvent<TodoTask> editorSaveEvent)  {
        // a little bit hacking here
        try {
            Class<?> editorImplClass = Class.forName("com.vaadin.ui.components.grid.EditorImpl");
            Field edited = editorImplClass.getDeclaredField("edited");
            edited.setAccessible(true);

            return (TodoTask) edited.get(editorSaveEvent.getSource());
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
           log.error("could not find edited field into Editor", e);
           return null;
        }
    }
}
