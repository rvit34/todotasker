package org.beejee.tasks.todo.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
@Entity(name = "UserRoles")
@NoArgsConstructor
@Getter
public class UserRole {

    public static UserRole ADMIN = new UserRole(0,"ROLE_ADMIN");
    public static UserRole USER = new UserRole(1,"ROLE_USER");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    private String name;

    private UserRole(long id, String name){
        this.id = id;
        this.name = name;
    }

    public UserRole(String name) {
        this.name = name;
    }
}
