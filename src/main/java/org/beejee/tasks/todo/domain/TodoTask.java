package org.beejee.tasks.todo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
@Entity(name = "TodoTasks")
@Getter
@NoArgsConstructor
public class TodoTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @Setter
    private String description;

    @Column(nullable = false)
    private String email;

    @ManyToOne
    @JoinColumn(name = "user_name")
    private User user;

    @Setter
    private boolean completed;

    private String imagePath;

    public boolean isCompleted(){
        return completed;
    }

    public TodoTask(String description, String email, User user, boolean completed) {
       this(description, email, user, null, completed);
    }

    public TodoTask(String description, String email, User user, String imagePath, boolean completed) {
        this.description = description;
        this.email = email;
        this.user = user;
        this.imagePath = imagePath;
        this.completed = completed;
    }

}
