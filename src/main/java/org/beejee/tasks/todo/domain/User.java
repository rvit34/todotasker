package org.beejee.tasks.todo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
@Entity(name = "Users")
@Getter
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "name",unique = true,nullable = false,length = 16)
    private String name = null;

    @Column(length = 32) // for ordinary users pass might not be set
    private String password = null;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="USER_ROLE",
            joinColumns=@JoinColumn(name="USER_NAME", referencedColumnName="name"),
            inverseJoinColumns=@JoinColumn(name="ROLE_ID", referencedColumnName="id"))
    private Set<UserRole> roles = new HashSet<>();


    public User(String name, UserRole role){
        this(name, null, role);
    }

    public User(String name, String password, UserRole role) {
        this.name = name;
        this.password = password;
        roles.add(role);
    }

    public void addRole(UserRole role){
        roles.add(role);
    }
}
