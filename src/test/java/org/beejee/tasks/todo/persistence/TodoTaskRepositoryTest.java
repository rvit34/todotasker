package org.beejee.tasks.todo.persistence;

import org.beejee.tasks.todo.TestFactory;
import org.beejee.tasks.todo.domain.TodoTask;
import org.beejee.tasks.todo.domain.User;
import org.beejee.tasks.todo.domain.UserRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TodoTaskRepositoryTest {

    @Autowired
    TodoTaskRepository todoTaskRepository;

    @Autowired
    UserRoleReporitory userRoleReporitory;

    @Autowired
    UserRepository userRepository;

    TestFactory testFactory = new TestFactory();

    int pageSize = 3;

    List<TodoTask> tasks;

    @Before
    public void setUp() throws Exception {

        UserRole role = new UserRole("SIMPLE_USER");
        List<User> users = testFactory.createUsers(6, role);

        userRoleReporitory.save(role);
        userRepository.save(users);

        tasks = testFactory.createTasks(6, 2, users);

        todoTaskRepository.save(tasks);

    }

    @Test
    public void testSearchAllTasksOrderedByUserName(){

        Pageable pageRequest = new PageRequest(0,pageSize);

        Page<TodoTask> firstPage = todoTaskRepository.findAllByOrderByUserName(pageRequest);

        Assert.assertTrue(firstPage.hasContent());
        Assert.assertThat(firstPage.getSize(),is(pageSize));

        Object[] actual = firstPage.getContent().stream().map(TodoTask::getUser).map(User::getName).toArray();
        Object[] expected = tasks.subList(0, pageSize).stream().map(TodoTask::getUser).map(User::getName).toArray();

        Assert.assertArrayEquals(expected,actual);

        Assert.assertTrue(firstPage.hasNext());

        Page<TodoTask> secondPage = todoTaskRepository.findAllByOrderByUserName(pageRequest.next());

        Assert.assertTrue(secondPage.hasContent());
        Assert.assertThat(secondPage.getSize(),is(pageSize));

        actual = secondPage.getContent().stream().map(TodoTask::getUser).map(User::getName).toArray();
        expected = tasks.subList(pageSize, pageSize*2).stream().map(TodoTask::getUser).map(User::getName).toArray();

        Assert.assertArrayEquals(expected,actual);

        Assert.assertFalse(secondPage.hasNext());

    }

    @Test
    public void testSearchAllTasksOrderedByEmail(){

        Pageable pageRequest = new PageRequest(0,pageSize);

        Page<TodoTask> firstPage = todoTaskRepository.findAllByOrderByEmail(pageRequest);

        Assert.assertTrue(firstPage.hasContent());
        Assert.assertThat(firstPage.getSize(),is(pageSize));

        Object[] actual = firstPage.getContent().stream().map(TodoTask::getEmail).toArray();
        Object[] expected = tasks.subList(0, pageSize).stream().map(TodoTask::getEmail).toArray();

        Assert.assertArrayEquals(expected,actual);

        Assert.assertTrue(firstPage.hasNext());

        Page<TodoTask> secondPage = todoTaskRepository.findAllByOrderByEmail(pageRequest.next());

        Assert.assertTrue(secondPage.hasContent());
        Assert.assertThat(secondPage.getSize(),is(pageSize));

        actual = secondPage.getContent().stream().map(TodoTask::getEmail).toArray();
        expected = tasks.subList(pageSize, pageSize*2).stream().map(TodoTask::getEmail).toArray();

        Assert.assertArrayEquals(expected,actual);

        Assert.assertFalse(secondPage.hasNext());

    }

    @Test
    public void testSearchAllCompletedTasks(){

        Pageable pageRequest = new PageRequest(1,pageSize);

        Page<TodoTask> firstPage = todoTaskRepository
                                     .findAllByOrderByCompleted(pageRequest);

        Object[] actual = firstPage.getContent().stream().map(TodoTask::isCompleted).skip(1).toArray();

        Assert.assertTrue(actual.length == 2);
        Assert.assertArrayEquals(actual, new Boolean[]{true,true});

    }



}