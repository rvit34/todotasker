package org.beejee.tasks.todo;

import org.apache.commons.lang.RandomStringUtils;
import org.beejee.tasks.todo.domain.TodoTask;
import org.beejee.tasks.todo.domain.User;
import org.beejee.tasks.todo.domain.UserRole;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vitaly Romashkin on 08.07.2017.
 */
public class TestFactory {

    public List<User> createUsers(int count, UserRole role){
        List<User> users = new ArrayList<>(count);
        User testUser;
        for (int i = 0;i<count;i++){
            testUser = new User("user#".concat(String.valueOf(i+1)),generatePassword(),role);
            users.add(testUser);
        }
        Collections.reverse(users);

        return users;
    }

    public List<TodoTask> createTasks(int taskCount,int completedCount,List<User> users){

        int userCount = users.size();

        if (taskCount != userCount) throw new IllegalArgumentException("userCount != taskCount");

        TodoTask testTask;
        User user;


        List<TodoTask> tasks = new ArrayList<>(taskCount);

        for (int i = 0;i<taskCount;i++,completedCount--){
            user = users.get(i);
            testTask = new TodoTask(
                generateRandomDescription(),
                user.getName().concat("@mail.com"),
                user,
                completedCount > 0
            );

            tasks.add(testTask);
        }

        Collections.reverse(tasks);

        return tasks;
    }

    private String generatePassword(){
        return RandomStringUtils.random(32, 0, 20, true, true, "qw32rfHIJk9iQ8Ud7h0X".toCharArray());
    }

    private String generateRandomDescription(){
        return RandomStringUtils.randomAlphabetic(64);
    }



}
