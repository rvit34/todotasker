package org.beejee.tasks.todo.service.impl;

import org.beejee.tasks.todo.TestFactory;
import org.beejee.tasks.todo.domain.TodoTask;
import org.beejee.tasks.todo.domain.User;
import org.beejee.tasks.todo.domain.UserRole;
import org.beejee.tasks.todo.persistence.TodoTaskRepository;
import org.beejee.tasks.todo.persistence.UserRepository;
import org.beejee.tasks.todo.persistence.UserRoleReporitory;
import org.beejee.tasks.todo.service.TodoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Vitaly Romashkin on 09.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TodoServiceImplTest {

    @Autowired
    private TodoService todoService;

    @Autowired
    private UserRoleReporitory userRoleReporitory;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TodoTaskRepository todoTaskRepository;

    private User testUser;

    @Before
    public void setUp() throws Exception {

        UserRole testUserRole = new UserRole("ROLE_USER");
        testUser = new User("test","123",testUserRole);

        userRoleReporitory.save(testUserRole);
        userRepository.save(testUser);
    }

    @Test
    public void testThatNewTaskIsAdded(){

        TodoTask newTask = new TodoTask("Need to finish the test task until next monday","user@mail.com",testUser,false);

        newTask = todoService.addNewTodo(newTask);
        Assert.assertNotNull(todoTaskRepository.findOne(newTask.getId()));

    }

    @Test
    public void testThatTaskDescriptionWasUpdated(){

        // :given
        TodoTask cleanTask = new TodoTask("Need to clean home until tomorrow","cleaner@mail.com",testUser,false);

        cleanTask = todoService.addNewTodo(cleanTask);
        String newDescription = "Need to clean home today";

        // :when
        todoService.updateDescription(newDescription, cleanTask.getId());
        TodoTask result = todoTaskRepository.findOne(cleanTask.getId());

        // :then
        Assert.assertNotNull(result);
        Assert.assertThat(result.getDescription(),is(newDescription));

    }

    @Test
    public void testPagination(){

        // :given
        TestFactory testFactory = new TestFactory();
        List<User> users = testFactory.createUsers(10, testUser.getRoles().iterator().next());
        List<TodoTask> tasks = testFactory.createTasks(10, 5, users);

        userRepository.save(users);
        todoTaskRepository.save(tasks);

        // :when
        Page<TodoTask> firstMicroPage = todoService.getAllTodos(0, 1);
        Page<TodoTask> secondBigPage = todoService.getAllTodos(0, 10);
        Page<TodoTask> thirdMiddlePage = todoService.getAllTodos(2,2);

        // :then
        Assert.assertThat(firstMicroPage.getSize(),is(1));
        Assert.assertThat(firstMicroPage.getContent().get(0).getDescription(),is(tasks.get(0).getDescription()));

        Assert.assertTrue(secondBigPage.isFirst());
        Assert.assertTrue(secondBigPage.isLast());

        Object[] actual = secondBigPage.getContent().stream().map(TodoTask::getDescription).toArray();
        Object[] expected = tasks.stream().map(TodoTask::getDescription).toArray();

        Assert.assertArrayEquals(expected,actual);

        Assert.assertThat(thirdMiddlePage.getSize(),is(2));
        Assert.assertThat(thirdMiddlePage.getContent().get(0).getDescription(),is(tasks.get(4).getDescription()));
        Assert.assertThat(thirdMiddlePage.getContent().get(1).getDescription(),is(tasks.get(5).getDescription()));
    }

    // note: imho no need to test another todoService.getAllTodosOrderBy** methods due to the same methods were already tested in TodoTaskRepositoryTest
}