# A Simple Web-based fullstack Java Todo tasker

## Description
Here you can find my implementation of test task I've done for BeeJee company.
This application provides an simple web user interface to add todo tasks in a list.
Some of requirements were:

* todo task should contain: user name, email, task description, status and image;

* todo list should support sorting and pagination;

* image should support JPG/GIF/PNG format with no more than 320х240 pixel resolution (in case of user attempts to load an higher resolution image the app has to scale it down correctly to 320х240);

* implement preview mode before saving new task (without page reloading);

* implement user roles where ordinary user can show list and create new tasks while admin user is able to edit task description and change task status.


## Tech stack
Frontend: Vaadin

Backend: Spring(Boot,Security,JPA), H2DB

## Prerequisites
You need JDK8(or higher) installed and maven build tool

### Build
Build the project from project root directory :
>./mvn clean package

### Launch on your local machine
Run java process with packaged fat jar and go to localhost:8080 in your browser:
>java -jar ./target/todo-0.0.1-SNAPSHOT.jar

### Working demo
You can try a working instance deployed on heroku (slow loading):

https://ancient-meadow-23324.herokuapp.com/todo - public todo list

https://ancient-meadow-23324.herokuapp.com/admin/todo - todo list with admin functions (edit description,change task status). Use admin/123 to login
